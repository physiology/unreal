set "ue_home=C:\Program Files\Epic Games"
set "build_to=C:\Programming\builds\pulse-4.2-unreal-"
@echo off

call:BuildPlugin 4.26
call:BuildPlugin 4.27
call:BuildPlugin 5.0
call:BuildPlugin 5.1
call:BuildPlugin 5.2
call:BuildPlugin 5.3

echo.&goto:eof
::::::::::::::::::::::
:: Function Section ::
::::::::::::::::::::::

:BuildPlugin
@echo on
rmdir "%build_to%%~1" /Q/S
call "%ue_home%\UE_%~1\Engine\Build\BatchFiles\RunUAT.bat" BuildPlugin -Plugin=%cd%\PulsePhysiologyEngine\PulsePhysiologyEngine.uplugin -Package=%build_to%%~1 -Rocket
