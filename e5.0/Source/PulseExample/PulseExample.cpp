/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseExample.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PulseExample, "PulseExample" );
