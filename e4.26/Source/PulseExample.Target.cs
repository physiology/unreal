/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

using UnrealBuildTool;
using System.Collections.Generic;

public class PulseExampleTarget : TargetRules
{
	public PulseExampleTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "PulseExample" } );
	}
}
