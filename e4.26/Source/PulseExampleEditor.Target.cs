/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

using UnrealBuildTool;
using System.Collections.Generic;

public class PulseExampleEditorTarget : TargetRules
{
	public PulseExampleEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "PulseExample" } );
	}
}
