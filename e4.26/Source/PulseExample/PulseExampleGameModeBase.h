/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PulseExampleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PULSEEXAMPLE_API APulseExampleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
