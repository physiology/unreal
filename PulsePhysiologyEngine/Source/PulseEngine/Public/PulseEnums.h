﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "PulseEnums.generated.h"

// Keep in sync with Pulse C++ API

/** Pulse file format. */
UENUM(BlueprintType)
enum class EPulseFileFormat : uint8
{
  AutoDetect,
  Json,
  Binary,
};

/** Pulse model type. */
UENUM(BlueprintType)
enum class EPulseModelType : uint8
{
  HumanAdultWholeBody = 0,
  HumanAdultVentilationMechanics,
  HumanAdultHemodynamics,
};
