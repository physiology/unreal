﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "Engine/AssetUserData.h"
#include "PulseState.generated.h"

class UAssetImportData;
class UAssetUserData;

/**
 * A Pulse state, imported as an Unreal asset.
 * 
 * @note Only states serialized as JSON are supported for now.
 */
UCLASS(BlueprintType)
class PULSEENGINE_API UPulseState : public UObject, public IInterface_AssetUserData
{
  GENERATED_BODY()

public:

  UPulseState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

#if WITH_EDITORONLY_DATA
  /** Import data. */
  UPROPERTY(VisibleAnywhere, Instanced, Category = "ImportSettings")
  UAssetImportData* AssetImportData = nullptr;
#endif

  /** State JSON. */
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "State")
  FString Json;

protected:

  /** User data stored with the asset. */
  UPROPERTY(EditAnywhere, Instanced, Category = "State", AdvancedDisplay)
  TArray<UAssetUserData*> AssetUserData;

  // UObject interface
public:
  void PostInitProperties() override;
  void Serialize(FArchive& Ar) override;

  // IInterface_AssetUserData interface
public:
  void AddAssetUserData(UAssetUserData* InUserData) override;
  UAssetUserData* GetAssetUserDataOfClass(TSubclassOf<UAssetUserData> InUserDataClass) override;
  const TArray<UAssetUserData*>* GetAssetUserDataArray() const override;
  void RemoveUserDataOfClass(TSubclassOf<UAssetUserData> InUserDataClass) override;
};
