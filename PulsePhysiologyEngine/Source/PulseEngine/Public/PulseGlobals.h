﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"

PULSEENGINE_API DECLARE_LOG_CATEGORY_EXTERN(LogPulse, Log, All);

#if defined(__clang__)
  #ifndef PULSE_INCLUDES_START
    #define PULSE_INCLUDES_START \
      THIRD_PARTY_INCLUDES_START
  #endif
  #ifndef PULSE_INCLUDES_END
    #define PULSE_INCLUDES_END \
      THIRD_PARTY_INCLUDES_END
  #endif
#else
  #ifndef PULSE_INCLUDES_START
    #define PULSE_INCLUDES_START \
      THIRD_PARTY_INCLUDES_START \
      __pragma(warning(disable: 4263)) \
      __pragma(warning(disable: 4264))
  #endif
  #ifndef PULSE_INCLUDES_END
    #define PULSE_INCLUDES_END \
      THIRD_PARTY_INCLUDES_END
  #endif
#endif
