/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "PulsePhysiologyEngine.h"

#include "ExamplePulseComponent.generated.h"

class SEGasSubstanceQuantity;
class UExamplePulseComponent;

USTRUCT(BlueprintType)
struct FVitalsMonitorData
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  float ECG_mV;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 HeartRate_bpm;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  float ArterialPressure_mmHg;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 MeanArterialPressure_mmHg;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 SystolicArterialPressure_mmHg;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 DiastolicArterialPressure_mmHg;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  float OxygenSaturation;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 EndTidalCO2Pressure_mmHg;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  int32 RespirationRate_bpm;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  float CoreTemperature_F;
  UPROPERTY(BlueprintReadOnly, Category="Pulse")
  float CarinaCO2PartialPressure_mmHg;

#if WITH_PULSE
  bool ReadyData(PhysiologyEngine* pulseEngine);
  SEGasSubstanceQuantity* CarinaO2;
#endif
};

#if WITH_PULSE
/**
* A class to check if the patient is dead
* Identifying patient death is highly contextual
* Depending on what has happened to the patient, you will identify death differently
* In this example, we demonstrate several possible
* death checks for the 3 main causes of death on the battle field
* Hemorrhage, Pneumothorax, Airway Obstruction
* - Always check for irreversable state
* - Cardiovascular Collapse
*    Note, Cardiovascular Collapse is recoverable in Cardiac Arrest, but it not when hemorrhaging
* - Patient HR reaches the patient maximum
* - Patient has brain oxygen deficit for an extended amount of time
* - Patient has myocardium oxygen deficit for an extended amount of time
* - Systolic Blood Pressure is under 60mmHg
* - SpO2 is under 85 for 3 min
* Note this is an example, times/events may vary depending on scenario and your SME
*/
class DeathChecker : public SEEventHandler
{
public:
  DeathChecker(PhysiologyEngine* pulseEngine);
  ~DeathChecker() = default;

  void HandleEvent(eEvent e, bool active, const SEScalarTime* simTime) override;

  bool IsPatientDead();

  std::string GetReason() { return m_Reason; }
private:
  PhysiologyEngine* m_PulseEngine;
  bool  m_IrreversableState = false;
  bool  m_CardiovascularCollapse = false;

  bool  m_BrainO2Deficit = false;
  float m_StartBrainO2Deficit_s = 0;

  bool  m_MyocardiumO2Deficit = false;
  float m_StartMyocardiumO2Deficit_s = 0;

  bool  m_SpO2Deficit = false;
  float m_StartSpO2Deficit_s = 0;

  std::string m_Reason="";
};
#endif

/**
* A component with customized, application specific logic 
* running an instance of the Pulse Physiology Engine.
*/
UCLASS(Meta = (BlueprintSpawnableComponent))
class PULSEENGINE_API UExamplePulseComponent : public UPulsePhysiologyEngine
{
  GENERATED_BODY()

  /** Perform actions related to an IED explosion. */
  UFUNCTION(BlueprintPure, Category="Pulse")
  bool IED_Explosion();

  /** Get data associated with a vitals monitor. */
  UFUNCTION(BlueprintPure, Category="Pulse")
  FVitalsMonitorData& GetVitalMonitorData();

protected:
#if	WITH_PULSE
  /** Called when a new simulation has been initialized and is ready to start. */
  bool ReadyEngine() override;
  bool IsPatientDead() override;
#endif
  bool m_IED_Exploded = false;
  FVitalsMonitorData m_VitalsMonitorData;
#if WITH_PULSE
  TUniquePtr<DeathChecker> m_DeathChecker;
#endif
};
