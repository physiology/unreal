﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "Trace/Trace.h"

UE_TRACE_CHANNEL_EXTERN(PulseChannel, PULSEENGINE_API);
