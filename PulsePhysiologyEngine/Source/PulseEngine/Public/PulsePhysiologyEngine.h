﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UObject/ObjectMacros.h"

#include "PulseEnums.h"
#include "PulseGlobals.h"

#if WITH_PULSE
PULSE_INCLUDES_START
#include "engine/PulseEngine.h"
#include "cdm/engine/SEEventManager.h"
PULSE_INCLUDES_END
#endif

#include "PulsePhysiologyEngine.generated.h"

class LoggerForward;
class PhysiologyEngine;
class UPulseState;
class UPulsePhysiologyEngine;

/** Delegate fired when the Pulse simulation is ready to start. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPulseOnSimulationReady, UPulsePhysiologyEngine*, Component);

/** Delegate fired every time the Pulse simulation is ticked. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPulseOnSimulationStep, UPulsePhysiologyEngine*, Component, float, SimTime_s);

/** Delegate fired when the Pulse simulation is reset. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPulseOnSimulationReset, UPulsePhysiologyEngine*, Component);

/** Delegate fired when the Pulse patient is dead. */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPulseOnPatientDeath, UPulsePhysiologyEngine*, Component, float, SimTime_s);

/**
 * A component that runs an instance of the Pulse Physiology Engine,
 * with support for data requests and actions.
 */
UCLASS(Meta=(BlueprintSpawnableComponent))
class PULSEENGINE_API UPulsePhysiologyEngine : public UActorComponent
{
  GENERATED_BODY()

public:
  UPulsePhysiologyEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
  UPulsePhysiologyEngine(FVTableHelper& Helper);
  ~UPulsePhysiologyEngine();

  /** Event fired when the Pulse simulation is initialized and ready to start. */
  UPROPERTY(BlueprintAssignable)
  FPulseOnSimulationReady OnSimulationReady;

  /** Event fired every time the Pulse simulation is ticked. */
  UPROPERTY(BlueprintAssignable)
  FPulseOnSimulationStep OnSimulationStep;

  /** Event fired when the Pulse simulation is reset. */
  UPROPERTY(BlueprintAssignable)
  FPulseOnSimulationReset OnSimulationReset;

  /** Event fired every time the Pulse simulation is ticked. */
  UPROPERTY(BlueprintAssignable)
  FPulseOnPatientDeath OnPatientDeath;

  /** Defines the type of engine that will be initialized. */
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pulse")
  EPulseModelType ModelType = EPulseModelType::HumanAdultWholeBody;

  /** Resets the component, clearing the active simulation. */
  UFUNCTION(BlueprintCallable, Category = "Pulse")
  void Reset();

protected:
  enum class State
  {
    Uninitialized =0,
    Ready,
    Active
  };
  State  m_EngineState;
  double m_TimeStep_s;
  double m_CurrentTime_s;
#if  WITH_PULSE
  /** Pulse engine instance. */
  TUniquePtr<PhysiologyEngine> m_PulseEngine;
  TUniquePtr<LoggerForward> m_PulseLogger;
#endif

public:

  /** Gets whether the simulation was initialized correctly. */
  UFUNCTION(BlueprintPure, Category = "Pulse")
  bool IsSimulationInitialized() const;

  /** Gets the simulation time (in seconds, only valid if a simulation is initialized). */
  UFUNCTION(BlueprintPure, Category = "Pulse")
  float GetSimulationTime() const;

  /** Initializes the simulation from the provided state (will reset any active simulation). */
  UFUNCTION(BlueprintCallable, Category = "Pulse", Meta = (UnsafeDuringActorConstruction = "true"))
  bool InitializeFromState(UPulseState* NewState);

  /** Initializes the simulation from the provided state file (will reset any active simulation). */
  UFUNCTION(BlueprintCallable, Category = "Pulse", Meta = (UnsafeDuringActorConstruction = "true", AdvancedDisplay = 1))
  bool InitializeFromStateFile(const FString& FilePath, EPulseFileFormat Format = EPulseFileFormat::AutoDetect);

  /** Initializes the simulation from the provided JSON-serialized state data (will reset any active simulation). */
  UFUNCTION(BlueprintCallable, Category = "Pulse", Meta = (UnsafeDuringActorConstruction = "true"))
  bool InitializeFromStateJson(const FString& StateJson);

  /** Initializes the simulation from the provided binary-serialized state data (will reset any active simulation). */
  bool InitializeFromStateBinary(const TArray<uint8>& StateBinary);

  /** Initializes the simulation from the provided patient file (will reset any active simulation). */
  UFUNCTION(BlueprintCallable, Category = "Pulse", Meta = (UnsafeDuringActorConstruction = "true", AdvancedDisplay = 1))
  bool InitializeFromPatientFile(const FString& FilePath, EPulseFileFormat Format = EPulseFileFormat::AutoDetect);

  /** Initializes the simulation from the provided JSON-serialized patient data (will reset any active simulation). */
  UFUNCTION(BlueprintCallable, Category = "Pulse", Meta = (UnsafeDuringActorConstruction = "true"))
  bool InitializeFromPatientJson(const FString& PatientJson);

  /** Initializes the simulation from the provided binary-serialized patient data (will reset any active simulation). */
  bool InitializeFromPatientBinary(const TArray<uint8>& PatientBinary);

#if  WITH_PULSE
protected:
  /** Called when a new simulation has been initialized and is ready to start. */
  virtual bool ReadyEngine();
  virtual bool IsPatientDead() { return false; }
#endif

  // UActorComponent interface
public:
  void Activate(bool bReset = false) override;
  void Deactivate() override;
  void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
  void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
