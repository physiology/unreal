﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "Misc/Guid.h"

/** Pulse object version. */
namespace FPulseCustomVersion
{
  enum Type
  {
    /** Initial version. */
    Initial = 0,

    // -----<new versions can be added above this line>-------------------------------------------------
    VersionPlusOne,
    LatestVersion = VersionPlusOne - 1
  };

  /** GUID for the custom version. */
  extern PULSEENGINE_API const FGuid GUID;
}
