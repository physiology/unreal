﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseState.h"
#include "PulseCustomVersion.h"

#include "EditorFramework/AssetImportData.h"

UPulseState::UPulseState(const FObjectInitializer& ObjectInitializer)
  : Super(ObjectInitializer)
{}

void UPulseState::PostInitProperties()
{
  UObject::PostInitProperties();

#if WITH_EDITORONLY_DATA
  if (!IsTemplate())
  {
    AssetImportData = NewObject<UAssetImportData>(this, TEXT("AssetImportData"));
  }
#endif
}

void UPulseState::Serialize(FArchive& Ar)
{
  Super::Serialize(Ar);
  Ar.UsingCustomVersion(FPulseCustomVersion::GUID);
}

void UPulseState::AddAssetUserData(UAssetUserData* InUserData)
{
  if (InUserData != nullptr)
  {
    if (UAssetUserData* ExistingData = GetAssetUserDataOfClass(InUserData->GetClass()))
    {
      AssetUserData.Remove(ExistingData);
    }
    AssetUserData.Add(InUserData);
  }
}

UAssetUserData* UPulseState::GetAssetUserDataOfClass(const TSubclassOf<UAssetUserData> InUserDataClass)
{
  for (UAssetUserData* Data : AssetUserData)
  {
    if (Data != nullptr && Data->IsA(InUserDataClass))
    {
      return Data;
    }
  }
  return nullptr;
}

const TArray<UAssetUserData*>* UPulseState::GetAssetUserDataArray() const
{
  return &AssetUserData;
}

void UPulseState::RemoveUserDataOfClass(const TSubclassOf<UAssetUserData> InUserDataClass)
{
  for (int32 i = 0; i < AssetUserData.Num(); i++)
  {
    UAssetUserData* Data = AssetUserData[i];
    if (Data != nullptr && Data->IsA(InUserDataClass))
    {
      AssetUserData.RemoveAt(i, 1, false);
      return;
    }
  }
}
