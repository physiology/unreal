/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulsePhysiologyEngine.h"
#include "PulseGlobals.h"
#include "PulseState.h"

#include "Misc/FileHelper.h"
#include "Misc/PackageName.h"
#include "Misc/Paths.h"

#if WITH_PULSE
PULSE_INCLUDES_START
#include "engine/PulseEngine.h"
#include "cdm/engine/SEEngineTracker.h"
#include "cdm/engine/SEEventManager.h"
PULSE_INCLUDES_END
#endif

namespace PulseCompUtils
{
#if WITH_PULSE
  TOptional<eSerializationFormat> DetectSerializationFormat(const FString& FilePath, const EPulseFileFormat FileFormat)
  {
    switch (FileFormat)
    {
      case EPulseFileFormat::AutoDetect:
      {
        const FString Extension = FPaths::GetExtension(FilePath).ToLower();
        if (Extension == TEXT("json"))
        {
          return eSerializationFormat::JSON;
        }
        if (Extension == TEXT("pbb"))
        {
          return eSerializationFormat::BINARY;
        }
        return TOptional<eSerializationFormat>();
      }

      case EPulseFileFormat::Json:
        return eSerializationFormat::JSON;

      case EPulseFileFormat::Binary:
        return eSerializationFormat::BINARY;

      default:
        checkNoEntry();
        break;
    }
    return TOptional<eSerializationFormat>();
  }

  class PulseLogger : public LoggerForward
  {
  public:
    virtual void ForwardDebug(const std::string& msg) { UE_LOG(LogPulse, Warning, TEXT("[DEBUG] %s"), UTF8_TO_TCHAR(msg.c_str())); }
    virtual void ForwardInfo(const std::string& msg) { UE_LOG(LogPulse, Warning, TEXT("[INFO] %s"), UTF8_TO_TCHAR(msg.c_str())); }
    virtual void ForwardWarning(const std::string& msg) { UE_LOG(LogPulse, Warning, TEXT("[WARN] %s"), UTF8_TO_TCHAR(msg.c_str())); }
    virtual void ForwardError(const std::string& msg) { UE_LOG(LogPulse, Warning, TEXT("[ERROR] %s"), UTF8_TO_TCHAR(msg.c_str())); }
    virtual void ForwardFatal(const std::string& msg) { UE_LOG(LogPulse, Warning, TEXT("[FATAL] %s"), UTF8_TO_TCHAR(msg.c_str())); }
  };
#endif

  FString ResolveFilePath(const FString& FilePath)
  {
    // Resolve the file path
    // (to support both rooted paths like /Game and filesystem paths)
    FString ResolvedPath;
    if (FPackageName::IsValidPath(FilePath))
    {
      FPackageName::TryConvertGameRelativePackagePathToLocalPath(FilePath, ResolvedPath);
    }

    if (ResolvedPath.IsEmpty())
    {
      ResolvedPath = FilePath;
    }

    return ResolvedPath;
  }
}

/////////////////////////////////////////////////////////////////////
UPulsePhysiologyEngine::UPulsePhysiologyEngine(const FObjectInitializer& ObjectInitializer)
  : Super(ObjectInitializer)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
  PrimaryComponentTick.bCanEverTick = static_cast<bool>(WITH_PULSE);
  PrimaryComponentTick.bStartWithTickEnabled = false;

  UE_SET_LOG_VERBOSITY(LogPulse, Verbose);

#if WITH_PULSE
  m_PulseEngine.Reset(CreatePulseEngine(static_cast<eModelType>(ModelType)).release());
  m_PulseLogger.Reset(new PulseCompUtils::PulseLogger());
  m_TimeStep_s = m_PulseEngine->GetTimeStep(TimeUnit::s);
  m_PulseEngine->GetLogger()->LogToConsole(false);
  m_PulseEngine->GetLogger()->AddForward(m_PulseLogger.Get());
#if NO_LOGGING
  m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Fatal);
#else
  switch (UE_GET_LOG_VERBOSITY(LogPulse))
  {
  case ELogVerbosity::VeryVerbose:
    m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Debug);
    break;
  case ELogVerbosity::Verbose:
    m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Info);
    break;
  case ELogVerbosity::Display:
  case ELogVerbosity::Log:
  case ELogVerbosity::Warning:
    m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Warn);
    break;
  case ELogVerbosity::Fatal:
  case ELogVerbosity::Error:
    m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Error);
    break;
  case ELogVerbosity::NoLogging:
  default:
    m_PulseEngine->GetLogger()->SetLogLevel(Logger::Level::Fatal);
    break;
  }
#endif
#endif
}

UPulsePhysiologyEngine::UPulsePhysiologyEngine(FVTableHelper& Helper)
  : Super(Helper)
{}

UPulsePhysiologyEngine::~UPulsePhysiologyEngine() = default;

void UPulsePhysiologyEngine::Activate(const bool bReset)
{
  if (m_EngineState != UPulsePhysiologyEngine::State::Ready)
  {
    UE_LOG(LogPulse, Warning,
      TEXT("Trying to activate component %s that hasn't been initialized, aborting."), *GetName());
    return;
  }

  m_EngineState = UPulsePhysiologyEngine::State::Active;
  Super::Activate(bReset);
}

void UPulsePhysiologyEngine::Reset()
{
  OnSimulationReset.Broadcast(this);
  Deactivate();
}

void UPulsePhysiologyEngine::Deactivate()
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
  Super::Deactivate();
}

bool UPulsePhysiologyEngine::ReadyEngine()
{
  m_CurrentTime_s = 0;
  m_EngineState = UPulsePhysiologyEngine::State::Ready;
  OnSimulationReady.Broadcast(this);
  return true;
}

void UPulsePhysiologyEngine::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
  Super::EndPlay(EndPlayReason);
}

bool UPulsePhysiologyEngine::IsSimulationInitialized() const
{
  return m_EngineState != UPulsePhysiologyEngine::State::Uninitialized;
}

float UPulsePhysiologyEngine::GetSimulationTime() const
{
#if WITH_PULSE
  if (m_EngineState == UPulsePhysiologyEngine::State::Active)
  {
    return m_PulseEngine->GetSimulationTime(TimeUnit::s);
  }
#endif
  return 0.0f;
}

bool UPulsePhysiologyEngine::InitializeFromState(UPulseState* NewState)
{
#if WITH_PULSE
  if (NewState != nullptr)
  {
    return InitializeFromStateJson(NewState->Json);
  }
#endif
  UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine from state asset %s"), *GetNameSafe(NewState));
  return false;
}

bool UPulsePhysiologyEngine::InitializeFromStateFile(const FString& FilePath, const EPulseFileFormat Format)
{
#if WITH_PULSE
  const TOptional<eSerializationFormat> PulseSerializationFormat =
    PulseCompUtils::DetectSerializationFormat(FilePath, Format);

  if (!PulseSerializationFormat.IsSet())
  {
    UE_LOG(LogPulse, Warning, TEXT("Trying to load unsupported state file %s"), *FilePath);
    return false;
  }

  if (PulseSerializationFormat.GetValue() == eSerializationFormat::JSON)
  {
    FString FileContent;
    if (FFileHelper::LoadFileToString(FileContent, *PulseCompUtils::ResolveFilePath(FilePath)))
    {
      return InitializeFromStateJson(FileContent);
    }
  }
  else if (PulseSerializationFormat.GetValue() == eSerializationFormat::BINARY)
  {
    TArray<uint8> FileContent;
    if (FFileHelper::LoadFileToArray(FileContent, *PulseCompUtils::ResolveFilePath(FilePath)))
    {
      return InitializeFromStateBinary(FileContent);
    }
  }
  else
  {
    checkNoEntry();
    UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine from state file %s"), *FilePath);
  }
#endif
  // If we're here, initialization failed
  return false;
}

bool UPulsePhysiologyEngine::InitializeFromStateJson(const FString& StateJson)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
#if WITH_PULSE
  try
  {
    if (m_PulseEngine->SerializeFromString(TCHAR_TO_UTF8(*StateJson), eSerializationFormat::JSON))
    {
      if(ReadyEngine())
        m_EngineState = UPulsePhysiologyEngine::State::Ready;
    }
    else
    {
      UE_LOG(LogPulse, Error, TEXT("Failed to deserialize state file"));
    }
  }
  catch (const std::exception& Exception)
  {
    UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine: %s"), UTF8_TO_TCHAR(Exception.what()));
  }
#endif
  return m_EngineState == UPulsePhysiologyEngine::State::Ready;
}

bool UPulsePhysiologyEngine::InitializeFromStateBinary(const TArray<uint8>& StateBinary)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
#if WITH_PULSE
  try
  {
    if (m_PulseEngine->SerializeFromString(
      std::string(reinterpret_cast<const char*>(StateBinary.GetData()), StateBinary.Num()),
      eSerializationFormat::BINARY))
    {
      if (ReadyEngine())
        m_EngineState = UPulsePhysiologyEngine::State::Ready;
    }
    else
    {
      UE_LOG(LogPulse, Error, TEXT("Failed to deserialize state file"));
    }
  }
  catch (const std::exception& Exception)
  {
    UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine: %s"), UTF8_TO_TCHAR(Exception.what()));
  }
#endif
  return m_EngineState == UPulsePhysiologyEngine::State::Ready;
}

bool UPulsePhysiologyEngine::InitializeFromPatientJson(const FString& PatientJson)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
#if WITH_PULSE
  try
  {
    if (m_PulseEngine->InitializeEngine(TCHAR_TO_UTF8(*PatientJson), eSerializationFormat::JSON))
    {
      if (ReadyEngine())
        m_EngineState = UPulsePhysiologyEngine::State::Ready;
    }
  }
  catch (const std::exception& Exception)
  {
    UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine: %s"), UTF8_TO_TCHAR(Exception.what()));
  }
#endif
  return m_EngineState == UPulsePhysiologyEngine::State::Ready;
}

bool UPulsePhysiologyEngine::InitializeFromPatientBinary(const TArray<uint8>& PatientBinary)
{
  m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
#if WITH_PULSE
  try
  {
    if (m_PulseEngine->InitializeEngine(
      std::string(reinterpret_cast<const char*>(PatientBinary.GetData()), PatientBinary.Num()),
      eSerializationFormat::BINARY))
    {
      if (ReadyEngine())
        m_EngineState = UPulsePhysiologyEngine::State::Ready;
    }
  }
  catch (const std::exception& Exception)
  {
    UE_LOG(LogPulse, Error, TEXT("Failed to initialize engine: %s"), UTF8_TO_TCHAR(Exception.what()));
  }
#endif
  return m_EngineState == UPulsePhysiologyEngine::State::Ready;
}

bool UPulsePhysiologyEngine::InitializeFromPatientFile(const FString& FilePath, const EPulseFileFormat Format)
{
#if WITH_PULSE
  const TOptional<eSerializationFormat> PulseSerializationFormat =
    PulseCompUtils::DetectSerializationFormat(FilePath, Format);

  if (!PulseSerializationFormat.IsSet())
  {
    UE_LOG(LogPulse, Warning, TEXT("Trying to load unsupported patient file %s"), *FilePath);
    return false;
  }

  if (PulseSerializationFormat.GetValue() == eSerializationFormat::JSON)
  {
    FString FileContent;
    if (FFileHelper::LoadFileToString(FileContent, *PulseCompUtils::ResolveFilePath(FilePath)))
    {
      return InitializeFromPatientJson(FileContent);
    }
  }
  else if (PulseSerializationFormat.GetValue() == eSerializationFormat::BINARY)
  {
    TArray<uint8> FileContent;
    if (FFileHelper::LoadFileToArray(FileContent, *PulseCompUtils::ResolveFilePath(FilePath)))
    {
      return InitializeFromStateBinary(FileContent);
    }
  }
  else
  {
    checkNoEntry();
  }
#endif
  return false;
}

void UPulsePhysiologyEngine::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  if (IsSimulationInitialized())
  {
    m_CurrentTime_s += DeltaTime;
    if (m_CurrentTime_s >= m_TimeStep_s)
    {
#if WITH_PULSE
      int count = (int)(m_CurrentTime_s / m_TimeStep_s);
      if (count > 2) count = 2; // Push off remaining updats until next frame
      for (int i = 0; i < count; i++)
      {
        if (!m_PulseEngine->AdvanceModelTime())
        {
          UE_LOG(LogPulse, Error, TEXT("Unable to advance engine time"));
          m_EngineState = UPulsePhysiologyEngine::State::Uninitialized;
          break;
        }
        if (IsPatientDead())
        {
          OnPatientDeath.Broadcast(this, m_PulseEngine->GetSimulationTime(TimeUnit::s));
          Reset();
          break;
        }
        else
          OnSimulationStep.Broadcast(this, m_PulseEngine->GetSimulationTime(TimeUnit::s));
      }
      m_CurrentTime_s = m_CurrentTime_s - (count * m_TimeStep_s);
#endif
    }
  }
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
