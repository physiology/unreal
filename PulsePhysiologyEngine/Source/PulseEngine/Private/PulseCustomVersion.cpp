﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseCustomVersion.h"

#include "Serialization/CustomVersion.h"

const FGuid FPulseCustomVersion::GUID(TEXT("3a3b6ca1-3fd5-4e4c-8044-217fc60967eb"));

// Register the custom version with core
FCustomVersionRegistration GRegisterPulseCustomVersion(FPulseCustomVersion::GUID, FPulseCustomVersion::LatestVersion, TEXT("Pulse"));
