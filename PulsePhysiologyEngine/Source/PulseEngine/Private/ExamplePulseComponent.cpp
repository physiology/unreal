/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "ExamplePulseComponent.h"


#if WITH_PULSE
PULSE_INCLUDES_START
#include "cdm/patient/SEPatient.h"
// Actions
#include "cdm/patient/actions/SEAirwayObstruction.h"
#include "cdm/patient/actions/SEHemorrhage.h"
#include "cdm/patient/actions/SEAcuteStress.h"
#include "cdm/patient/actions/SETensionPneumothorax.h"
// Compartments
#include "cdm/compartment/SECompartmentManager.h"
#include "cdm/compartment/fluid/SEGasCompartment.h"
#include "cdm/compartment/substances/SEGasSubstanceQuantity.h"
#include "cdm/substance/SESubstance.h"
#include "cdm/substance/SESubstanceManager.h"
// Systems
#include "cdm/system/equipment/electrocardiogram/SEElectroCardioGram.h"
#include "cdm/system/physiology/SEBloodChemistrySystem.h"
#include "cdm/system/physiology/SECardiovascularSystem.h"
#include "cdm/system/physiology/SEEnergySystem.h"
#include "cdm/system/physiology/SERespiratorySystem.h"
// Scalars
#include "cdm/properties/SEScalar0To1.h"
#include "cdm/properties/SEScalarFrequency.h"
#include "cdm/properties/SEScalarElectricPotential.h"
#include "cdm/properties/SEScalarPressure.h"
#include "cdm/properties/SEScalarTemperature.h"
PULSE_INCLUDES_END
#endif

bool UExamplePulseComponent::ReadyEngine()
{
#if WITH_PULSE
  if (!m_VitalsMonitorData.ReadyData(m_PulseEngine.Get()))
    return false;
  m_DeathChecker.Reset(new DeathChecker(m_PulseEngine.Get()));
  m_PulseEngine->GetEventManager().ForwardEvents(m_DeathChecker.Get());
#endif

  return UPulsePhysiologyEngine::ReadyEngine();
}

bool UExamplePulseComponent::IED_Explosion()
{// I am just making up this action/severity combination
 // You could also add a distance from explosion and scale severity by that...
  if (!m_IED_Exploded)
  {
    m_IED_Exploded = true;

#if WITH_PULSE
    SEAcuteStress stress;
    stress.GetSeverity().SetValue(0.85);
    m_PulseEngine->ProcessAction(stress);

    SEAirwayObstruction aobs;
    aobs.GetSeverity().SetValue(0.25);
    m_PulseEngine->ProcessAction(aobs);

    SETensionPneumothorax pneumo;
    pneumo.SetSide(eSide::Left);
    pneumo.SetType(eGate::Open);
    pneumo.GetSeverity().SetValue(0.17);
    m_PulseEngine->ProcessAction(pneumo);

    SEHemorrhage rightLeg;
    rightLeg.SetCompartment(eHemorrhage_Compartment::RightLeg);
    rightLeg.GetSeverity().SetValue(0.95);
    m_PulseEngine->ProcessAction(rightLeg);

    SEHemorrhage leftLeg;
    leftLeg.SetCompartment(eHemorrhage_Compartment::LeftLeg);
    leftLeg.GetSeverity().SetValue(0.85);
    m_PulseEngine->ProcessAction(leftLeg);
#endif

    return true;
  }
  return false;
}

bool FVitalsMonitorData::ReadyData(PhysiologyEngine* pulseEngine)
{
#if WITH_PULSE
  const SESubstance* co2 = pulseEngine->GetSubstanceManager().GetSubstance("CarbonDioxide");
  if (co2 == nullptr)
  {
    UE_LOG(LogPulse, Warning, TEXT("[INFO] Unable to get cardon dioxide."));
    return false;
  }
  const SEGasCompartment* carina = pulseEngine->GetCompartments().GetGasCompartment(pulse::PulmonaryCompartment::Carina);
  if (carina == nullptr)
  {
    UE_LOG(LogPulse, Warning, TEXT("[INFO] Unable to get carina."));
    return false;
  }
  CarinaO2 = carina->GetSubstanceQuantity(*co2);
  if (CarinaO2 == nullptr)
  {
    UE_LOG(LogPulse, Warning, TEXT("[INFO] Unable to get carina oxygen."));
    return false;
  }
#endif
  return true;
}
FVitalsMonitorData& UExamplePulseComponent::GetVitalMonitorData()
{
  if (m_EngineState != UPulsePhysiologyEngine::State::Active)
  {
    m_VitalsMonitorData.ECG_mV = 0;
    m_VitalsMonitorData.HeartRate_bpm = 0;
    m_VitalsMonitorData.ArterialPressure_mmHg = 0;
    m_VitalsMonitorData.MeanArterialPressure_mmHg = 0;
    m_VitalsMonitorData.SystolicArterialPressure_mmHg = 0;
    m_VitalsMonitorData.DiastolicArterialPressure_mmHg = 0;
    m_VitalsMonitorData.OxygenSaturation = 0;
    m_VitalsMonitorData.EndTidalCO2Pressure_mmHg = 0;
    m_VitalsMonitorData.RespirationRate_bpm = 0;
    m_VitalsMonitorData.CoreTemperature_F = 0;
    m_VitalsMonitorData.CarinaCO2PartialPressure_mmHg = 0;
  }
  else
  {
#if WITH_PULSE
    m_VitalsMonitorData.ECG_mV =
      m_PulseEngine->GetElectroCardioGram()->GetLead3ElectricPotential(ElectricPotentialUnit::mV);
    m_VitalsMonitorData.HeartRate_bpm =
      m_PulseEngine->GetCardiovascularSystem()->GetHeartRate(FrequencyUnit::Per_min);
    m_VitalsMonitorData.ArterialPressure_mmHg =
      m_PulseEngine->GetCardiovascularSystem()->GetArterialPressure(PressureUnit::mmHg);
    m_VitalsMonitorData.MeanArterialPressure_mmHg =
      m_PulseEngine->GetCardiovascularSystem()->GetMeanArterialPressure(PressureUnit::mmHg);
    m_VitalsMonitorData.SystolicArterialPressure_mmHg =
      m_PulseEngine->GetCardiovascularSystem()->GetSystolicArterialPressure(PressureUnit::mmHg);
    m_VitalsMonitorData.DiastolicArterialPressure_mmHg =
      m_PulseEngine->GetCardiovascularSystem()->GetDiastolicArterialPressure(PressureUnit::mmHg);
    m_VitalsMonitorData.OxygenSaturation =
      m_PulseEngine->GetBloodChemistrySystem()->GetOxygenSaturation();
    m_VitalsMonitorData.EndTidalCO2Pressure_mmHg =
      m_PulseEngine->GetRespiratorySystem()->GetEndTidalCarbonDioxidePressure(PressureUnit::mmHg);
    m_VitalsMonitorData.RespirationRate_bpm =
      m_PulseEngine->GetRespiratorySystem()->GetRespirationRate(FrequencyUnit::Per_min);
    m_VitalsMonitorData.CoreTemperature_F =
      m_PulseEngine->GetEnergySystem()->GetCoreTemperature(TemperatureUnit::F);
    m_VitalsMonitorData.CarinaCO2PartialPressure_mmHg = m_VitalsMonitorData.CarinaO2->GetPartialPressure(PressureUnit::mmHg);
#endif
  }

  return m_VitalsMonitorData;
}

bool UExamplePulseComponent::IsPatientDead()
{
#if WITH_PULSE
  if (m_DeathChecker->IsPatientDead())
  {
    UE_LOG(LogPulse, Warning, TEXT("[INFO] %s"), UTF8_TO_TCHAR(m_DeathChecker->GetReason().c_str()));
    return true;
  }
#endif
  return false;
}
#if WITH_PULSE
DeathChecker::DeathChecker(PhysiologyEngine* pe) : m_PulseEngine(pe) {}
void DeathChecker::HandleEvent(eEvent e, bool active, const SEScalarTime* simTime)
{
  if (e == eEvent::IrreversibleState)
  {
    m_IrreversableState = active;
    return;
  }

  if (e == eEvent::CardiovascularCollapse)
  {
    m_CardiovascularCollapse = active;
    return;
  }

  if (e == eEvent::BrainOxygenDeficit)
  {
    if (active)
    {
      if (!m_BrainO2Deficit)
      {
        m_BrainO2Deficit = true;
        m_StartBrainO2Deficit_s = simTime->GetValue(TimeUnit::s);
      }
    }
    else
    {
      m_BrainO2Deficit = false;
      m_StartBrainO2Deficit_s = 0;
    }
  }

  if (e == eEvent::MyocardiumOxygenDeficit)
  {
    if (active)
    {
      if (!m_MyocardiumO2Deficit)
      {
        m_MyocardiumO2Deficit = true;
        m_StartMyocardiumO2Deficit_s = simTime->GetValue(TimeUnit::s);
      }
    }
    else
    {
      m_MyocardiumO2Deficit = false;
      m_StartMyocardiumO2Deficit_s = 0;
    }
  }
}
bool DeathChecker::IsPatientDead()
{
  double currentSimTime_s = m_PulseEngine->GetSimulationTime(TimeUnit::s);
  if (m_IrreversableState)
  {
    m_Reason = "Patient died from irreversable state @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }
  if (m_CardiovascularCollapse)
  {
    m_Reason = "Patient died from cardiovascular collapse @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }
  if (m_PulseEngine->GetCardiovascularSystem()->GetHeartRate(FrequencyUnit::Per_min) >=
      m_PulseEngine->GetInitialPatient().GetHeartRateMaximum(FrequencyUnit::Per_min))
  {
    m_Reason = "Patient died from reaching max hr of "
      + pulse::cdm::to_string(m_PulseEngine->GetInitialPatient().GetHeartRateMaximum(FrequencyUnit::Per_min))
      + "bpm @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }
  if (m_BrainO2Deficit && (currentSimTime_s - m_StartBrainO2Deficit_s) > 180)
  {
    m_Reason = "Patient died from brain O2 deficit of 180s @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }
  if (m_MyocardiumO2Deficit && (currentSimTime_s - m_StartMyocardiumO2Deficit_s) > 180)
  {
    m_Reason = "Patient died from myocardium O2 deficit of 180s @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }

  // SpO2 Check
  if (m_PulseEngine->GetBloodChemistrySystem()->GetOxygenSaturation() <= 0.85)
  {
    if (!m_SpO2Deficit)
    {
      m_SpO2Deficit = true;
      m_StartSpO2Deficit_s = currentSimTime_s;
    }
    if ((currentSimTime_s - m_StartSpO2Deficit_s) > 140)
    {
      m_Reason = "Patient died from SpO2 < 85 for 140s @" + pulse::cdm::to_string(currentSimTime_s) + "s";
      return true;
    }
  }
  else
    m_SpO2Deficit = false;

  // Systolic Blood Pressure (mmHg) Check
  if (m_PulseEngine->GetCardiovascularSystem()->GetSystolicArterialPressure(PressureUnit::mmHg) < 60)
  {
    m_Reason = "Patient died from a SBP < 60 @" + pulse::cdm::to_string(currentSimTime_s) + "s";
    return true;
  }
  return false;
}
#endif
