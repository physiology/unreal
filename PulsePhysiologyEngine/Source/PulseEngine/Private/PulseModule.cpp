/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseModule.h"

IMPLEMENT_MODULE(FPulseModule, PulseEngine);
