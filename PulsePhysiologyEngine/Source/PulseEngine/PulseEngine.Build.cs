/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

using System;
using System.IO;

namespace UnrealBuildTool.Rules
{
  public class PulseEngine : ModuleRules
  {
    public PulseEngine(ReadOnlyTargetRules Target) : base(Target)
    {
      PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
      //bLegacyPublicIncludePaths = false;
      //OptimizeCode = CodeOptimization.Never;
      bEnableExceptions = true;
      //bUseRTTI = true;
      //IWYUSupport = IWYUSupport.None;

      PublicDependencyModuleNames.AddRange(new[]
      {
      "ApplicationCore",
      "Engine",
      "Core",
      "CoreUObject",
      "TraceLog",
      });

      //OptimizeCode = CodeOptimization.Never;
      PublicIncludePaths.AddRange(new string[]
      {
        // ... add any public include paths required here ...
        IncludePath,
      });

      PrivateIncludePaths.AddRange(new string[]
      {
      // ... add any private include paths required here ...
      });

      PrivateDependencyModuleNames.AddRange(new string[]
      {
        // ... add private dependencies that you statically link with here ...
      });

      DynamicallyLoadedModuleNames.AddRange(new string[]
      {
        // ... add any modules that your module loads dynamically here ...
      });

      if (LoadLib(Target))
      {
        PublicDefinitions.Add("WITH_PULSE=1");

        // Stage Pulse content files
        string ContentPath = Path.Combine(PluginDirectory, "Content");
        foreach (string Folder in Directory.GetDirectories(ContentPath))
        {
          foreach (string File in Directory.GetFiles(Folder))
          {
            string FileExtension = Path.GetExtension(File);
            if (FileExtension.Equals(".json", StringComparison.OrdinalIgnoreCase)
                || FileExtension.Equals(".pbb", StringComparison.OrdinalIgnoreCase))
            {
              RuntimeDependencies.Add(File, StagedFileType.NonUFS);
            }
          }
        }
      }
      else
      {
        PublicDefinitions.Add("WITH_PULSE=0");
      }
    }

    public bool LoadLib(ReadOnlyTargetRules Target)
    {
      bool IsLibrarySupported = false;
      if (Target.Platform == UnrealTargetPlatform.Win64)
      {
        IsLibrarySupported = true;

        string PlatformString = Target.Platform.ToString();

        string BinDLLPath = Path.Combine(BinariesPath, PlatformString, "Pulse.dll");
        string BinDLLManifPath = Path.Combine(BinariesPath, PlatformString, "Pulse.dll.manifest");
        string ThirdPartyDllPath = Path.Combine(LibraryPath, PlatformString, "Pulse.dll");
        string ThirdPartyDllManifPath = Path.Combine(LibraryPath, PlatformString, "Pulse.dll.manifest");

        // Copy third party DLLs to all these paths
        RuntimeDependencies.Add(BinDLLPath, ThirdPartyDllPath);
        RuntimeDependencies.Add(BinDLLManifPath, ThirdPartyDllManifPath);
        
        // This will copy dlls if not copied already
        CopyToBinaries(ThirdPartyDllPath);
        CopyToBinaries(ThirdPartyDllManifPath);

        //Lib
        PublicAdditionalLibraries.Add(Path.Combine(LibraryPath, PlatformString, "Pulse.lib"));
        System.Console.WriteLine("plugin using lib at " + Path.Combine(LibraryPath, PlatformString, "Pulse.lib"));

        if (!IsEnginePlugin())
        {
          RuntimeDependencies.Add("$(BinaryOutputDir)/Pulse.dll");
        }
        
      }
      return IsLibrarySupported;
    }

    private void CopyToBinaries(string Filepath)
    {
      string binariesDir = Path.Combine(BinariesPath, Target.Platform.ToString());
      string filename = Path.GetFileName(Filepath);

      if (!Directory.Exists(binariesDir))
        Directory.CreateDirectory(binariesDir);

      if (!File.Exists(Path.Combine(binariesDir, filename)))
        File.Copy(Filepath, Path.Combine(binariesDir, filename), true);
    }

    private bool IsEnginePlugin()
    {
      return Path.GetFullPath(ModuleDirectory).EndsWith("Engine\\Plugins\\Runtime\\PulsePhysiologyEngine\\Source\\PulseEngine");
    }

    private string ModulePath { get { return ModuleDirectory; } }

    private string ThirdPartyPath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(EngineDirectory, "Source/ThirdParty"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ModulePath, "../ThirdParty/"));
        }
      }
    }

    private string BinariesPath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(EngineDirectory, "Binaries/ThirdParty/PulseSDK"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ModulePath, "../../Binaries/"));
        }
      }
    }

    private string LibraryPath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseSDK/Lib"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseSDK/Lib"));
        }
      }
    }

    private string IncludePath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseSDK/Include/pulse"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseSDK/Include/pulse"));
        }
      }
    }
  }
}
