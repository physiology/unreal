﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseStateFactory.h"
#include "PulseState.h"

#include "EditorFramework/AssetImportData.h"
#include "Editor.h"

UPulseStateFactory::UPulseStateFactory(const FObjectInitializer& ObjectInitializer)
  : Super(ObjectInitializer)
{
  SupportedClass = UPulseState::StaticClass();
  bCreateNew = false;
  bEditorImport = true;
  bText = true;
  Formats.Add(TEXT("state;Pulse State"));
  Formats.Add(TEXT("json;Pulse State"));
}

bool UPulseStateFactory::FactoryCanImport(const FString& Filename)
{
  FString Extension = FPaths::GetExtension(Filename);
  if (Extension.IsEmpty())
  {
    return false;
  }

  Extension.ToLowerInline();
  TArray<FString> SupportedExtensions;
  GetSupportedFileExtensions(SupportedExtensions);
  return SupportedExtensions.Contains(Extension);
}

UObject* UPulseStateFactory::FactoryCreateText(UClass* InClass, UObject* InParent, const FName InName, const EObjectFlags Flags, UObject* Context, const TCHAR* Type, const TCHAR*& Buffer, const TCHAR* BufferEnd, FFeedbackContext* Warn, bool& bOutOperationCanceled)
{
  if (const UImportSubsystem* ImportSubsystem = GEditor->GetEditorSubsystem<UImportSubsystem>())
  {
    ImportSubsystem->OnAssetPreImport.Broadcast(this, InClass, InParent, InName, Type);
  }

  UPulseState* State =
    Cast<UPulseState>(CreateOrOverwriteAsset(InClass, InParent, InName, Flags));

  if (State != nullptr && State->AssetImportData != nullptr)
  {
    // TODO: try to parse/validate JSON?
    // TODO: compress JSON?
    State->Json = FString(BufferEnd - Buffer, Buffer);
    State->AssetImportData->Update(GetCurrentFilename());
  }

  if (const UImportSubsystem* ImportSubsystem = GEditor->GetEditorSubsystem<UImportSubsystem>())
  {
    ImportSubsystem->OnAssetPostImport.Broadcast(this, State);
  }

  return State;
}

bool UPulseStateFactory::CanReimport(UObject* Obj, TArray<FString>& OutFilenames)
{
  UPulseState* State = Cast<UPulseState>(Obj);
  if (State != nullptr && State->AssetImportData != nullptr)
  {
    State->AssetImportData->ExtractFilenames(OutFilenames);
  }
  return OutFilenames.Num() > 0;
}

void UPulseStateFactory::SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths)
{
  UPulseState* State = Cast<UPulseState>(Obj);
  if (State != nullptr
    && State->AssetImportData != nullptr
    && ensure(NewReimportPaths.Num() == 1))
  {
    State->AssetImportData->UpdateFilenameOnly(NewReimportPaths[0]);
  }
}

EReimportResult::Type UPulseStateFactory::Reimport(UObject* Obj)
{
  UPulseState* State = Cast<UPulseState>(Obj);
  if (State == nullptr)
  {
    return EReimportResult::Failed;
  }

  // Make sure file is valid and exists
  const FString Filename = State->AssetImportData->GetFirstFilename();
  if (Filename.Len() == 0 || IFileManager::Get().FileSize(*Filename) == INDEX_NONE)
  {
    return EReimportResult::Failed;
  }
  
  // Import again
  bool bOutCanceled = false;
  State = Cast<UPulseState>(ImportObject(Obj->GetClass(), Obj->GetOuter(), *Obj->GetName(),
    RF_Public | RF_Standalone | RF_Transactional, Filename, nullptr, bOutCanceled));

  if (bOutCanceled)
  {
    return EReimportResult::Cancelled;
  }

  if (State == nullptr)
  {
    return EReimportResult::Failed;
  }

  return EReimportResult::Succeeded;
}
