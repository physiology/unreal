/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class IAssetTypeActions;

class FPulseEditorModule final : public IModuleInterface
{
private:

  /** Registered asset type actions. */
  TArray<TSharedPtr<IAssetTypeActions>> AssetActions;

  void RegisterAssetActions();
  void UnregisterAssetActions();

  // IModuleInterface interface
public:
  void StartupModule() override;
  void ShutdownModule() override;
};
