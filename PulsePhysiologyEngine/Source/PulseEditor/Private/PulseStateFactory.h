﻿/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "EditorReimportHandler.h"
#include "Factories/Factory.h"
#include "PulseStateFactory.generated.h"

/**
 * (Re)Importer for Pulse states.
 * 
 * @note Only states serialized as JSON are supported for now.
 */
UCLASS()
class UPulseStateFactory : public UFactory, public FReimportHandler
{
  GENERATED_BODY()

public:

  UPulseStateFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

  // UFactory interface
public:
  bool FactoryCanImport(const FString& Filename) override;
  UObject* FactoryCreateText(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags, UObject* Context, const TCHAR* Type, const TCHAR*& Buffer, const TCHAR* BufferEnd, FFeedbackContext* Warn, bool& bOutOperationCanceled) override;

  // FReimportHandler interface
public:
  bool CanReimport(UObject* Obj, TArray<FString>& OutFilenames) override;
  void SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths) override;
  EReimportResult::Type Reimport(UObject* Obj) override;};
