/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseStateAssetActions.h"
#include "PulseState.h"

#include "EditorFramework/AssetImportData.h"

#define LOCTEXT_NAMESPACE "PulseStateAssetActions"

uint32 FPulseStateAssetActions::GetCategories()
{
  return EAssetTypeCategories::Misc;
}

FText FPulseStateAssetActions::GetName() const
{
  return LOCTEXT("Name", "Pulse State");
}

FColor FPulseStateAssetActions::GetTypeColor() const
{
  return FColor(194, 33, 38);
}

UClass* FPulseStateAssetActions::GetSupportedClass() const
{
  return UPulseState::StaticClass();
}

bool FPulseStateAssetActions::IsImportedAsset() const
{
  return true;
}

void FPulseStateAssetActions::GetResolvedSourceFilePaths(const TArray<UObject*>& TypeAssets, TArray<FString>& OutSourceFilePaths) const
{
  for (const UObject* Asset : TypeAssets)
  {
    const UPulseState* State = CastChecked<UPulseState>(Asset);
    if (State != nullptr && State->AssetImportData != nullptr)
    {
      State->AssetImportData->ExtractFilenames(OutSourceFilePaths);
    }
  }
}

#undef LOCTEXT_NAMESPACE
