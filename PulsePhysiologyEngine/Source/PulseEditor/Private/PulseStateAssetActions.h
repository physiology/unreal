/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#pragma once
#include "CoreMinimal.h"
#include "AssetTypeActions_Base.h"

/**
 * Asset type actions for Pulse state assets.
 */
class FPulseStateAssetActions final : public FAssetTypeActions_Base
{
public:
  // IAssetTypeActions interface
  uint32 GetCategories() override;
  FText GetName() const override;
  FColor GetTypeColor() const override;
  UClass* GetSupportedClass() const override;
  bool IsImportedAsset() const override;
  void GetResolvedSourceFilePaths(const TArray<UObject*>& TypeAssets, TArray<FString>& OutSourceFilePaths) const override;
};
