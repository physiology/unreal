/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

#include "PulseEditorModule.h"
#include "PulseStateAssetActions.h"

#include "AssetToolsModule.h"

IMPLEMENT_MODULE(FPulseEditorModule, PulseEditor);

void FPulseEditorModule::StartupModule()
{
  if (!IsRunningCommandlet())
  {
    RegisterAssetActions();
  }
}

void FPulseEditorModule::ShutdownModule()
{
  if (!IsRunningCommandlet())
  {
    UnregisterAssetActions();
  }
}

void FPulseEditorModule::RegisterAssetActions()
{
  if (const FAssetToolsModule* AssetToolsModule = FModuleManager::LoadModulePtr<FAssetToolsModule>(TEXT("AssetTools")))
  {
    AssetActions.Add(MakeShared<FPulseStateAssetActions>());

    IAssetTools& AssetTools = AssetToolsModule->Get();
    for (const TSharedPtr<IAssetTypeActions>& AssetAction : AssetActions)
    {
      AssetTools.RegisterAssetTypeActions(AssetAction.ToSharedRef());
    }
  }
}

void FPulseEditorModule::UnregisterAssetActions()
{
  if (const FAssetToolsModule* AssetToolsModule = FModuleManager::GetModulePtr<FAssetToolsModule>(TEXT("AssetTools")))
  {
    IAssetTools& AssetTools = AssetToolsModule->Get();
    for (const TSharedPtr<IAssetTypeActions>& Action : AssetActions)
    {
      AssetTools.UnregisterAssetTypeActions(Action.ToSharedRef());
    }
  }

  AssetActions.Reset();
}
