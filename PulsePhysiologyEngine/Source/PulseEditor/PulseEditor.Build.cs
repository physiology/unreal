/* Copyright 2022 by Kitware, Inc. All Rights Reserved.
   See accompanying NOTICE file for details.*/

using System.IO;

namespace UnrealBuildTool.Rules
{
  public class PulseEditor : ModuleRules
  {
    public PulseEditor(ReadOnlyTargetRules Target) : base(Target)
    {
      PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
      //bLegacyPublicIncludePaths = false;
      //bEnableExceptions = true;

      PublicIncludePaths.AddRange(new string[]
      {
        // ... add public include paths required here ...
      });

      PrivateIncludePaths.AddRange(new string[]
      {
        "PulseEngine/Private",
        IncludePath,
      });

      PublicDependencyModuleNames.AddRange(new string[]
      {
        // ... add other public dependencies that you statically link with here ...
        "Engine",
        "Core",
        "CoreUObject",
        "PulseEngine",
        //"PropertyEditor",
        //"Slate",
        //"SlateCore",
      });

      PrivateDependencyModuleNames.AddRange(new string[]
      {
        "UnrealEd",
        "BlueprintGraph",
        "AnimGraph",
        "AnimGraphRuntime",
        //"BodyState",
      });

      DynamicallyLoadedModuleNames.AddRange(new string[]
      {
        // ... add any modules that your module loads dynamically here ...
      });
    }

    private string ModulePath { get { return ModuleDirectory; } }

    private bool IsEnginePlugin()
    {
      return Path.GetFullPath(ModuleDirectory).EndsWith("Engine\\Plugins\\Runtime\\PulsePhysiologyEngine\\Source\\PulseEngine");
    }

    private string ThirdPartyPath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(EngineDirectory, "Source/ThirdParty"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ModulePath, "../ThirdParty/"));
        }
      }
    }

    private string IncludePath
    {
      get
      {
        if (IsEnginePlugin())
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseEngine/Include"));
        }
        else
        {
          return Path.GetFullPath(Path.Combine(ThirdPartyPath, "PulseSDK/Include"));
        }
      }
    }
  }
}
