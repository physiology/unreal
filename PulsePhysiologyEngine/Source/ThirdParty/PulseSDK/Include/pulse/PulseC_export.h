
#ifndef PULSEC_EXPORT_H
#define PULSEC_EXPORT_H

#ifdef PULSEC_STATIC_DEFINE
#  define PULSEC_EXPORT
#  define PULSEC_NO_EXPORT
#else
#  ifndef PULSEC_EXPORT
#    ifdef PulseC_EXPORTS
        /* We are building this library */
#      define PULSEC_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define PULSEC_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef PULSEC_NO_EXPORT
#    define PULSEC_NO_EXPORT 
#  endif
#endif

#ifndef PULSEC_DEPRECATED
#  define PULSEC_DEPRECATED __declspec(deprecated)
#endif

#ifndef PULSEC_DEPRECATED_EXPORT
#  define PULSEC_DEPRECATED_EXPORT PULSEC_EXPORT PULSEC_DEPRECATED
#endif

#ifndef PULSEC_DEPRECATED_NO_EXPORT
#  define PULSEC_DEPRECATED_NO_EXPORT PULSEC_NO_EXPORT PULSEC_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PULSEC_NO_DEPRECATED
#    define PULSEC_NO_DEPRECATED
#  endif
#endif

#endif /* PULSEC_EXPORT_H */
