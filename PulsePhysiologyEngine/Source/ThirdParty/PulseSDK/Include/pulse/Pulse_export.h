
#ifndef PULSE_EXPORT_H
#define PULSE_EXPORT_H

#ifdef PULSE_STATIC_DEFINE
#  define PULSE_EXPORT
#  define PULSE_NO_EXPORT
#else
#  ifndef PULSE_EXPORT
#    ifdef Pulse_EXPORTS
        /* We are building this library */
#      define PULSE_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define PULSE_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef PULSE_NO_EXPORT
#    define PULSE_NO_EXPORT 
#  endif
#endif

#ifndef PULSE_DEPRECATED
#  define PULSE_DEPRECATED __declspec(deprecated)
#endif

#ifndef PULSE_DEPRECATED_EXPORT
#  define PULSE_DEPRECATED_EXPORT PULSE_EXPORT PULSE_DEPRECATED
#endif

#ifndef PULSE_DEPRECATED_NO_EXPORT
#  define PULSE_DEPRECATED_NO_EXPORT PULSE_NO_EXPORT PULSE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PULSE_NO_DEPRECATED
#    define PULSE_NO_DEPRECATED
#  endif
#endif

#endif /* PULSE_EXPORT_H */
