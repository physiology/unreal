@echo off

:: Unreal 4 uses msvc 2017, so make sure you have that installed
:: But 4 and 5 both want Pulse libs made with 2019

call:SetupDirectory e4.26
call:SetupDirectory e4.27
call:SetupDirectory e5.0
call:SetupDirectory e5.1
call:SetupDirectory e5.2
call:SetupDirectory e5.3

echo.&goto:eof
::::::::::::::::::::::
:: Function Section ::
::::::::::::::::::::::

:SetupDirectory
@echo on
rmdir .\%~1\Plugins\PulsePhysiologyEngine /Q/S
xcopy .\PulsePhysiologyEngine\ .\%~1\Plugins\PulsePhysiologyEngine\ /Q/E



:: If we want to pull from a build
::xcopy %~2\install\include\pulse\ .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\pulse\ /Q/E
::mkdir .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\debug\
::xcopy %~2\install\lib\CommonDataModeld.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\debug\CommonDataModel.lib* /Q/Y
::xcopy %~2\install\lib\DataModelBindingsd.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\debug\DataModelBindings.lib* /Q/Y
::xcopy %~2\install\lib\libprotobufd.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\debug\libprotobuf.lib* /Q/Y
::xcopy %~2\install\lib\PulseEngined.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\debug\PulseEngine.lib* /Q/Y
::mkdir .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\release\
::xcopy %~2\install\lib\CommonDataModel.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\release\CommonDataModel.lib* /Q/Y
::xcopy %~2\install\lib\DataModelBindings.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\release\DataModelBindings.lib* /Q/Y
::xcopy %~2\install\lib\libprotobuf.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\release\libprotobuf.lib* /Q/Y
::xcopy %~2\install\lib\PulseEngine.lib .\%~1\Plugins\PulsePhysiologyEngine\Source\ThirdParty\PulseLibrary\lib\release\PulseEngine.lib* /Q/Y

@echo off